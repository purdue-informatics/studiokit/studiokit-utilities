﻿using System;
using System.Linq;

namespace StudioKit.Utilities;

public static class DateMath
{
	public static DateTime Max(params DateTime[] dates) => dates.Max();

	public static DateTime Min(params DateTime[] dates) => dates.Min();
}