using StudioKit.Utilities.Interfaces;
using System;

namespace StudioKit.Utilities;

/// <summary>
/// Implementation of <see cref="IDateTimeProvider"/> that can be edited at runtime
/// in order to test at different points in time.
/// </summary>
public class DynamicDateTimeProvider : IDateTimeProvider
{
	public DateTime UtcNow { get; set; }
}