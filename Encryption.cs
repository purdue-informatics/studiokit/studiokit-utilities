using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace StudioKit.Utilities;

/// <summary>
/// See http://stackoverflow.com/questions/202011/encrypt-decrypt-string-in-net
/// See https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.aes?view=net-6.0
/// </summary>
public static class Encryption
{
	// Preconfigured Password Key Derivation Parameters
	private const int SaltBitSize = 128;
	private const int Iterations = 10000;
	private static readonly SecureRandom Random = new();

	/// <summary>
	/// Secure salt generation. Returns salt for inclusion in payload
	/// See https://gist.github.com/jbtule/4336842#file-aesthenhmac-cs and
	/// https://stackoverflow.com/questions/202011/encrypt-and-decrypt-a-string
	/// TODO: incorporate that entire class here
	/// </summary>
	/// <param name="plaintext">The text to encrypt.</param>
	/// <param name="sharedSecret">A password used to generate a key for encryption.</param>
	/// <returns>An encrypted Base64 string</returns>
	public static (string, string) EncryptStringAes(string plaintext, string sharedSecret)
	{
		var generator = new Pkcs5S2ParametersGenerator();
		// Use Random Salt to minimize pre-generated weak password attacks.
		var salt = new byte[SaltBitSize / 8];
		Random.NextBytes(salt);

		generator.Init(
			PbeParametersGenerator.Pkcs5PasswordToBytes(sharedSecret.ToCharArray()),
			salt,
			Iterations);

		return (Convert.ToBase64String(salt), EncryptStringAes(plaintext, sharedSecret, salt));
	}

	/// <summary>
	/// Encrypt the given string using AES and encode as a Base64 string.
	/// The string can be decrypted using DecryptStringAES().
	/// The sharedSecret parameters must match.
	/// </summary>
	/// <param name="plainText">The text to encrypt.</param>
	/// <param name="sharedSecret">A password used to generate a key for encryption.</param>
	/// <param name="salt">The salt of the application.</param>
	public static string EncryptStringAes(string plainText, string sharedSecret, string salt)
	{
		return EncryptStringAes(plainText, sharedSecret, Encoding.UTF8.GetBytes(salt));
	}

	/// <summary>
	/// Encrypt the given string using AES and encode as a Base64 string.
	/// The string can be decrypted using DecryptStringAES().
	/// The sharedSecret parameters must match.
	/// </summary>
	/// <param name="plainText">The text to encrypt.</param>
	/// <param name="sharedSecret">A password used to generate a key for encryption.</param>
	/// <param name="salt">The salt of the application.</param>
	public static string EncryptStringAes(string plainText, string sharedSecret, byte[] salt)
	{
		if (string.IsNullOrEmpty(plainText))
			throw new ArgumentNullException(nameof(plainText));

		// generate the key from the shared secret and the salt
		var key = new Rfc2898DeriveBytes(sharedSecret, salt, Iterations, HashAlgorithmName.SHA1);

		// create an Aes object with the specified key and IV
		using var aesAlg = Aes.Create();
		aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
		aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
		// create a decryptor to perform the stream transform
		var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
		// create the streams used for encryption
		using var msEncrypt = new MemoryStream();
		using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
		{
			using var swEncrypt = new StreamWriter(csEncrypt);
			// write all data to the stream
			swEncrypt.Write(plainText);
		}

		var encryptedBytes = msEncrypt.ToArray();
		// encode the encrypted bytes to a Base64 string
		var encryptedString = Convert.ToBase64String(encryptedBytes);

		return encryptedString;
	}

	public static byte[] EncryptStringToBytesAes(string plainText, byte[] key, byte[] iv)
	{
		if (string.IsNullOrEmpty(plainText))
			throw new ArgumentNullException(nameof(plainText));
		if (key is not { Length: > 0 })
			throw new ArgumentNullException(nameof(key));
		if (iv is not { Length: > 0 })
			throw new ArgumentNullException(nameof(iv));

		// create an Aes object with the specified key and IV
		using var aesAlg = Aes.Create();
		aesAlg.Key = key;
		aesAlg.IV = iv;
		aesAlg.Padding = PaddingMode.None;
		// create a decryptor to perform the stream transform
		var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
		// create the streams used for encryption
		using var msEncrypt = new MemoryStream();
		using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
		{
			using var swEncrypt = new StreamWriter(csEncrypt);
			// write all data to the stream
			swEncrypt.Write(plainText);
		}

		var encryptedBytes = msEncrypt.ToArray();
		return encryptedBytes;
	}

	/// <summary>
	/// Decrypt the given string. Assumes the string was encrypted using
	/// EncryptStringAES(), using an identical sharedSecret.
	/// </summary>
	/// <param name="cipherText">The text to decrypt.</param>
	/// <param name="sharedSecret">A password used to generate a key for decryption.</param>
	/// <param name="salt">The salt of the application.</param>
	public static string DecryptStringAes(string cipherText, string sharedSecret, string salt)
	{
		return DecryptStringAes(cipherText, sharedSecret, Encoding.UTF8.GetBytes(salt));
	}

	/// <summary>
	/// Decrypt the given string. Assumes the string was encrypted using
	/// EncryptStringAES(), using an identical sharedSecret.
	/// </summary>
	/// <param name="cipherText">The text to decrypt.</param>
	/// <param name="sharedSecret">A password used to generate a key for decryption.</param>
	/// <param name="salt">The salt of the application.</param>
	public static string DecryptStringAes(string cipherText, string sharedSecret, byte[] salt)
	{
		if (string.IsNullOrEmpty(cipherText))
			throw new ArgumentNullException(nameof(cipherText));

		// decode from Base64
		var bytes = Convert.FromBase64String(cipherText);

		// generate the key from the shared secret and the salt
		var key = new Rfc2898DeriveBytes(sharedSecret, salt, Iterations, HashAlgorithmName.SHA1);

		// create a Aes object with the specified key and IV
		using var aesAlg = Aes.Create();
		aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
		aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
		// create a decryptor to perform the stream transform
		var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
		// create the streams used for decryption
		using var msDecrypt = new MemoryStream(bytes);
		using var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
		using var srDecrypt = new StreamReader(csDecrypt);
		// read the decrypted bytes from the decrypting stream and place them in a string
		var plaintext = srDecrypt.ReadToEnd();

		return plaintext;
	}

	public static string DecryptStringFromBytesAes(byte[] cipherText, byte[] key, byte[] iv)
	{
		if (cipherText is not { Length: > 0 })
			throw new ArgumentNullException(nameof(cipherText));
		if (key is not { Length: > 0 })
			throw new ArgumentNullException(nameof(key));
		if (iv is not { Length: > 0 })
			throw new ArgumentNullException(nameof(iv));

		// create a Aes object with the specified key and IV
		using var aesAlg = Aes.Create();
		aesAlg.Key = key;
		aesAlg.IV = iv;
		aesAlg.Padding = PaddingMode.None;
		// Create a decryptor to perform the stream transform.
		var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
		// Create the streams used for decryption.
		using var msDecrypt = new MemoryStream(cipherText);
		using var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
		using var srDecrypt = new StreamReader(csDecrypt);
		// Read the decrypted bytes from the decrypting stream
		// and place them in a string.
		var plaintext = srDecrypt.ReadToEnd();

		return plaintext;
	}

	public static byte[] HashWithSalt(string plaintext, byte[] salt)
	{
		var plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
		using var algorithm = SHA256.Create();
		var plaintextPlusSalt = new byte[plaintextBytes.Length + salt.Length];
		Buffer.BlockCopy(plaintextBytes, 0, plaintextPlusSalt, 0, plaintextBytes.Length);
		Buffer.BlockCopy(salt, 0, plaintextPlusSalt, plaintextBytes.Length, salt.Length);
		return algorithm.ComputeHash(plaintextPlusSalt);
	}

	public static byte[] CreateSalt(int length)
	{
		var salt = new byte[length];
		var random = new Random();
		random.NextBytes(salt);
		return salt;
	}

	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public static string CreateMD5HashedString(string input)
	{
		using var md5 = MD5.Create();
		var inputBytes = Encoding.ASCII.GetBytes(input);
		var hashBytes = md5.ComputeHash(inputBytes);
		return Convert.ToHexString(hashBytes);
	}
}