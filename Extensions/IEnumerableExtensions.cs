﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Utilities.Extensions;

// ReSharper disable InconsistentNaming
// It's an interface, Resharper!!!
public static class IEnumerableExtensions
	// ReSharper restore InconsistentNaming
{
	/// <summary>
	/// Checks whether <paramref name="enumerable"/> is null or empty.
	/// </summary>
	/// <typeparam name="T">The type of the <paramref name="enumerable"/>.</typeparam>
	/// <param name="enumerable">The <see cref="T:System.Collections.Generic.IEnumerable`1"/> to be checked.</param>
	/// <returns>True if <paramref name="enumerable"/> is null or empty, false otherwise.</returns>
	public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
	{
		return enumerable == null || !enumerable.Any<T>();
	}

	public static T MaxOrDefault<T>(this IEnumerable<T> source)
	{
		var src = source as T[] ?? source.ToArray();
		return src.Any() ? src.Max() : default(T);
	}

	/// <summary>
	/// A quick an easy way to get a distinct collection.
	/// Credit: http://stackoverflow.com/questions/520030/why-is-there-no-linq-method-to-return-distinct-values-by-a-predicate
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <typeparam name="TKey"></typeparam>
	/// <param name="source"></param>
	/// <param name="keySelector"></param>
	/// <returns></returns>
	public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source,
		Func<TSource, TKey> keySelector)
	{
		return source.DistinctBy(keySelector, EqualityComparer<TKey>.Default);
	}

	public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source,
		Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
	{
		if (source == null)
		{
			throw new ArgumentNullException("source");
		}

		if (keySelector == null)
		{
			throw new ArgumentNullException("keySelector");
		}

		if (comparer == null)
		{
			throw new ArgumentNullException("comparer");
		}

		return DistinctByImpl(source, keySelector, comparer);
	}

	private static IEnumerable<TSource> DistinctByImpl<TSource, TKey>(IEnumerable<TSource> source,
		Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
	{
		HashSet<TKey> knownKeys = new HashSet<TKey>(comparer);

		foreach (TSource element in source)
		{
			if (knownKeys.Add(keySelector(element)))
			{
				yield return element;
			}
		}
	}
}