using StudioKit.Utilities.Properties;
using System;

namespace StudioKit.Utilities.Extensions;

public static class IntExtensions
{
	/// <summary>
	/// Create an alpha label from an ordinal, similar to Excel column names, lower case.
	/// </summary>
	/// <param name="ordinal">An ordinal</param>
	/// <returns>The label</returns>
	public static string OrdinalToAlphaLabel(this int ordinal)
	{
		if (ordinal < 1) throw new ArgumentOutOfRangeException(nameof(ordinal), Strings.InvalidOrdinalValue);

		const int alphabetLength = 26;
		const int lowercaseANumber = 97; // a to z is 97 to 122

		// construct the label backwards
		// 1. find the current mod remainder, and add its letter
		// 2. recurse to the previous multiple of 26
		// 3. prepend next mod remainder letter
		// 4. repeat
		var label = "";
		var currentOrdinal = ordinal;
		while (currentOrdinal > 0)
		{
			var index = currentOrdinal - 1;
			var letterNumber = index % alphabetLength;
			var letterChar = (char)(letterNumber + lowercaseANumber);
			label = letterChar + label;
			currentOrdinal = (index - letterNumber) / alphabetLength;
		}

		return label;
	}

	/// <summary>
	/// Create a roman numeral label from an ordinal, lower case.
	/// </summary>
	/// <param name="ordinal">An ordinal</param>
	/// <returns>The label</returns>
	public static string OrdinalToRomanNumeralLabel(this int ordinal)
	{
		switch (ordinal)
		{
			case < 1:
				throw new ArgumentOutOfRangeException(nameof(ordinal), Strings.InvalidOrdinalValue);
			case > 3999:
				throw new ArgumentOutOfRangeException(nameof(ordinal), Strings.InvalidRomanNumeralValue);
		}

		var label = "";
		var currentOrdinal = ordinal;
		while (currentOrdinal > 0)
		{
			var (labelPart, ordinalDiff) = currentOrdinal switch
			{
				// it doesn't seem like this case can be hit, but it makes Rider happy
				< 1 => throw new ArgumentOutOfRangeException(nameof(ordinal), Strings.InvalidOrdinalValue),
				>= 1000 => ("m", -1000),
				>= 900 => ("cm", -900),
				>= 500 => ("d", -500),
				>= 400 => ("cd", -400),
				>= 100 => ("c", -100),
				>= 90 => ("xc", -90),
				>= 50 => ("l", -50),
				>= 40 => ("xl", -40),
				>= 10 => ("x", -10),
				>= 9 => ("ix", -9),
				>= 5 => ("v", -5),
				>= 4 => ("iv", -4),
				>= 1 => ("i", -1)
			};
			label += labelPart;
			currentOrdinal += ordinalDiff;
		}

		return label;
	}
}