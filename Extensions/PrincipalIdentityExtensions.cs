﻿using System.Security.Claims;
using System.Security.Principal;

namespace StudioKit.Utilities.Extensions;

public static class PrincipalIdentityExtensions
{
	public static string GetUserId(this IIdentity identity)
	{
		var claimsIdentity = identity as ClaimsIdentity;
		return claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
	}
}