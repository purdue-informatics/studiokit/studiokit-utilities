using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StudioKit.Utilities.Extensions;

public static class StringExtensions
{
	/// <summary>
	/// Compares the string against a given pattern.
	/// </summary>
	/// <param name="str">The string.</param>
	/// <param name="wildcard">The wildcard, where "*" means any sequence of characters, and "?" means any single character.</param>
	/// <returns><c>true</c> if the string matches the given pattern; otherwise <c>false</c>.</returns>
	public static bool Like(this string str, string wildcard)
	{
		var pattern = "^" + Regex.Escape(wildcard).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";
		return new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline).IsMatch(str);
	}

	/// <summary>
	/// A quick an easy way to clip a string to length without having to check for substring validation.
	/// </summary>
	/// <param name="str"></param>
	/// <param name="length"></param>
	/// <param name="appendEllipsis"></param>
	/// <returns></returns>
	public static string ClipLength(this string str, int length, bool appendEllipsis = true)
	{
		return str.Length <= length
			? str
			: str.Substring(0, length) + (appendEllipsis ? "..." : "");
	}

	public static bool IsValidEmailAddress(this string s)
	{
		var attribute = new EmailAddressAttribute();
		return attribute.IsValid(s);
	}

	/// <summary>
	/// ApiController name without "Controller"
	/// </summary>
	/// <param name="s">The type name</param>
	/// <returns></returns>
	public static string ApiifyControllerName(this string s)
	{
		return s.Substring(0, s.LastIndexOf("Controller", StringComparison.Ordinal));
	}

	public static string VanityName(this string s)
	{
		return Regex.Replace(s, @"[\s\.]+", "-");
	}

	public static bool IsValidFacebookProfileUrl(this string s)
	{
		return !string.IsNullOrWhiteSpace(s) && Uri.IsWellFormedUriString(s, UriKind.Absolute) &&
				Regex.IsMatch(s, @"https?://(www\.)?facebook\.com/.*");
	}

	public static bool IsValidLinkedInProfileUrl(this string s)
	{
		return !string.IsNullOrWhiteSpace(s) && Uri.IsWellFormedUriString(s, UriKind.Absolute) &&
				Regex.IsMatch(s, @"https?://(www\.)?linkedin\.com/.*");
	}

	public static bool IsValidTwitterProfileUrl(this string s)
	{
		return !string.IsNullOrWhiteSpace(s) && Uri.IsWellFormedUriString(s, UriKind.Absolute) &&
				Regex.IsMatch(s, @"https?://(www\.)?twitter\.com/.*");
	}

	public static string AddOrdinal(this string s)
	{
		int num = Convert.ToInt32(s);
		switch (num % 100)
		{
			case 11:
			case 12:
			case 13:
				return num + "th";
		}

		switch (num % 10)
		{
			case 1:
				return num + "st";

			case 2:
				return num + "nd";

			case 3:
				return num + "rd";

			default:
				return num + "th";
		}
	}

	public static string EnglishSeries(this List<string> source)
	{
		return ItemsToEnglishSeries(source);
	}

	public static string EnglishSeries(this IEnumerable<string> source)
	{
		return ItemsToEnglishSeries(source);
	}

	private static string ItemsToEnglishSeries(IEnumerable<string> source)
	{
		var items = source.ToList();
		return string.Join(", ", items.Take(items.Count - 1)) + (items.Count > 1 ? " and " : "") + items.LastOrDefault();
	}

	public static string UppercaseFirst(this string source)
	{
		if (string.IsNullOrEmpty(source)) return string.Empty;
		return char.ToUpperInvariant(source[0]) + source.Substring(1);
	}

	public static string ToCourseTermName(this string courseTerm)
	{
		var year = int.Parse(courseTerm.Substring(0, 4));
		var semesterCode = int.Parse(courseTerm.Substring(4));
		var name = "";
		switch (semesterCode)
		{
			case 10:
				name = "Fall";
				year--; // e.g. "201610" is for Fall 2015
				break;

			case 20:
				name = "Spring";
				break;

			case 30:
				name = "Summer";
				break;
		}

		return $"{name} {year}";
	}

	public static string ScopedDomainRegexPatten = @"(@)(.+)$";

	public static string GetDomain(this string scopedValue)
	{
		if (string.IsNullOrEmpty(scopedValue))
			return null;
		string domain = null;
		try
		{
			var match = Regex.Match(scopedValue, ScopedDomainRegexPatten);
			domain = match.Groups[2].Value;
		}
		catch (Exception)
		{
			//ignored
		}

		return domain;
	}

	/// <summary>
	/// Validate if a string has a valid domain scope, e.g. for email address or ePPN.
	/// Based on code from https://msdn.microsoft.com/en-us/library/01escwtf(v=vs.110).aspx
	/// </summary>
	/// <param name="testString">The email string</param>
	/// <returns></returns>
	public static bool HasValidDomainScope(this string testString)
	{
		if (string.IsNullOrEmpty(testString))
			return false;

		// Use IdnMapping class to convert Unicode domain names to ASCII
		string asciiString;
		try
		{
			asciiString = Regex.Replace(testString, ScopedDomainRegexPatten,
				ReplaceWithAsciiDomain,
				RegexOptions.None,
				TimeSpan.FromMilliseconds(200));
		}
		catch (ArgumentException)
		{
			return false; // catches ReplaceWithAsciiDomain exceptions
		}
		catch (RegexMatchTimeoutException)
		{
			return false;
		}

		// Return true if valid format
		try
		{
			return Regex.IsMatch(asciiString,
				@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
				@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
				RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
		}
		catch (RegexMatchTimeoutException)
		{
			return false;
		}
	}

	private static string ReplaceWithAsciiDomain(Match match)
	{
		// IdnMapping class with default property values
		var idn = new IdnMapping();
		var domainName = match.Groups[2].Value;
		domainName = idn.GetAscii(domainName);
		return match.Groups[1].Value + domainName;
	}

	public static T ToEnumStrict<T>(this string enumName) where T : struct
	{
		if (!typeof(T).IsEnum)
			throw new ArgumentException("T must be an enumerated type");
		if (enumName != null && Enum.TryParse(enumName, out T enumValue))
			return enumValue;
		throw new InvalidEnumArgumentException(nameof(enumName));
	}

	public static T? ToEnum<T>(this string enumName) where T : struct
	{
		if (enumName == null) return null;
		if (!typeof(T).IsEnum)
			throw new ArgumentException("T must be an enumerated type");
		if (Enum.TryParse(enumName, out T enumValue))
			return enumValue;
		throw new InvalidEnumArgumentException(nameof(enumName));
	}

	/// <summary>
	/// Pad the end of the given string with spaces until its length is a multiple of 16.
	/// </summary>
	/// <param name="value">A string to pad</param>
	/// <returns>A new string containing <paramref name="value"/>, padded to a length that is a multiple of 16.</returns>
	public static string SpacePad16(this string value)
	{
		var sb = new StringBuilder(value);
		while (sb.Length % 16 != 0)
		{
			sb.Append(' ');
		}

		return sb.ToString();
	}
}