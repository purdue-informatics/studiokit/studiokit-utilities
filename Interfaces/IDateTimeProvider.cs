﻿using System;

namespace StudioKit.Utilities.Interfaces;

public interface IDateTimeProvider
{
	DateTime UtcNow { get; }
}