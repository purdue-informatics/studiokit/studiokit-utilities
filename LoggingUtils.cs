using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;
using System;

namespace StudioKit.Utilities;

public static class LoggingUtils
{
	/// <summary>
	/// Logging configuration that mimics `HostingHostBuilderExtensions` in `Microsoft.Extensions.Hosting`,
	/// for use outside of the AspNetCore.App hosting pipeline (e.g. console app or LINQPad)
	/// </summary>
	/// <param name="configuration"></param>
	/// <returns></returns>
	public static ILoggerFactory ConfigureLoggerFactory(IConfigurationRoot configuration)
	{
		return LoggerFactory.Create(logging =>
		{
			var isWindows = OperatingSystem.IsWindows();
			// IMPORTANT: This needs to be added *before* configuration is loaded, this lets
			// the defaults be overridden by the configuration.
			if (isWindows)
			{
				// Default the EventLogLoggerProvider to warning or above
				logging.AddFilter<EventLogLoggerProvider>(level => level >= LogLevel.Warning);
			}

			logging.AddConfiguration(configuration.GetSection("Logging"));
			logging.AddConsole();
			logging.AddDebug();
			logging.AddEventSourceLogger();
			if (isWindows)
			{
				// Add the EventLogLoggerProvider on windows machines
				logging.AddEventLog();
			}

			logging.Configure(options =>
			{
				options.ActivityTrackingOptions =
					ActivityTrackingOptions.SpanId |
					ActivityTrackingOptions.TraceId |
					ActivityTrackingOptions.ParentId;
			});
		});
	}
}