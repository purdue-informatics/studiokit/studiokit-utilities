using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Tests;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Properties;
using System;

namespace StudioKit.Utilities.Tests.Extensions;

[TestClass]
public class IntExtensionsTests : BaseTest
{
	#region OrdinalToAlphaLabel

	[TestMethod]
	public void OrdinalToAlphaLabel_ShouldThrowIfLessThanOne()
	{
		Assert.Throws<ArgumentOutOfRangeException>(() =>
		{
			0.OrdinalToAlphaLabel();
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.InvalidOrdinalValue, "ordinal"));
	}

	[TestMethod]
	public void OrdinalToAlphaLabel_ShouldReturnLowerCaseAlphaLabel()
	{
		Assert.AreEqual("a", 1.OrdinalToAlphaLabel());
		Assert.AreEqual("b", 2.OrdinalToAlphaLabel());
		Assert.AreEqual("z", 26.OrdinalToAlphaLabel());
	}

	[TestMethod]
	public void OrdinalToAlphaLabel_ShouldReturnMultipleLettersForLargerValues()
	{
		Assert.AreEqual("aa", 27.OrdinalToAlphaLabel());
		Assert.AreEqual("ab", 28.OrdinalToAlphaLabel());
		Assert.AreEqual("az", (26 * 2).OrdinalToAlphaLabel());
		Assert.AreEqual("zz", (26 * 27).OrdinalToAlphaLabel());
	}

	#endregion OrdinalToAlphaLabel

	#region OrdinalToRomanNumeralLabel

	[TestMethod]
	public void OrdinalToRomanNumeralLabel_ShouldThrowIfInvalidValue()
	{
		Assert.Throws<ArgumentOutOfRangeException>(() =>
		{
			0.OrdinalToRomanNumeralLabel();
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.InvalidOrdinalValue, "ordinal"));

		Assert.Throws<ArgumentOutOfRangeException>(() =>
		{
			4000.OrdinalToRomanNumeralLabel();
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.InvalidRomanNumeralValue, "ordinal"));
	}

	[TestMethod]
	public void OrdinalToRomanNumeralLabel_ShouldReturnLowerCaseRomanNumeralLabel()
	{
		Assert.AreEqual("i", 1.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("ii", 2.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("iii", 3.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("iv", 4.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("v", 5.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("vi", 6.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("vii", 7.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("viii", 8.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("ix", 9.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("x", 10.OrdinalToRomanNumeralLabel());

		Assert.AreEqual("xi", 11.OrdinalToRomanNumeralLabel());

		Assert.AreEqual("xx", 20.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("xxx", 30.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("xl", 40.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("l", 50.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("lx", 60.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("lxx", 70.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("lxxx", 80.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("xc", 90.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("c", 100.OrdinalToRomanNumeralLabel());

		Assert.AreEqual("cd", 400.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("d", 500.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("cm", 900.OrdinalToRomanNumeralLabel());
		Assert.AreEqual("m", 1000.OrdinalToRomanNumeralLabel());

		Assert.AreEqual("mmmcmxcix", 3999.OrdinalToRomanNumeralLabel());
	}

	#endregion OrdinalToRomanNumeralLabel
}