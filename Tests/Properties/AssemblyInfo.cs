using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("StudioKit.Utilities.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("StudioKit.Utilities.Tests")]
[assembly: AssemblyCopyright("Copyright © 2024 Purdue University")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("47823b7c-b904-41b4-b3a2-eb4296a7e9ef")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]